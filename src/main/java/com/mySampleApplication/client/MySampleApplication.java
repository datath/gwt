package com.mySampleApplication.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;

public class MySampleApplication implements EntryPoint {


    private static MySampleApplicationServiceAsync servercall = GWT.create(MySampleApplicationService.class);

    public void onModuleLoad() {
        final Button button = new Button("Click me");
        final Label label = new Label();

        final DeckPanel deckPanel = new DeckPanel();
        deckPanel.setStyleName("gwt-DeckPanel");

         final Label firstPage = new Label("");
         final Label secondPage = new Label("");
         final Label thirdPage = new Label("");

        deckPanel.add(firstPage);
        deckPanel.add(secondPage);
        deckPanel.add(thirdPage);

        deckPanel.showWidget(0);

         HorizontalPanel buttonPanel = new HorizontalPanel();
        buttonPanel.setSize("300px", "20px");

        final Button firstButton = new Button("1");
        firstButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent clickEvent) {
                if (firstPage.getText().equals("")){
                    servercall.getMessage(firstButton.getText(),new MyAsyncCallback(firstPage));
                    deckPanel.showWidget(0);
                } else {
                    deckPanel.showWidget(0);
                }
            }
        });

        final Button secondButton = new Button("2");
        secondButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent clickEvent) {
                if (secondPage.getText().equals("")){
                    servercall.getMessage(secondButton.getText(),new MyAsyncCallback(secondPage));
                    deckPanel.showWidget(1);
                } else {
                    deckPanel.showWidget(1);
                }
            }
        });

        final Button thirdButton = new Button("3");
        thirdButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent clickEvent) {
                if (thirdPage.getText().equals("")){
                    servercall.getMessage(thirdButton.getText(),new MyAsyncCallback(thirdPage));
                    deckPanel.showWidget(2);
                } else {
                    deckPanel.showWidget(2);
                }

            }
        });


        button.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (label.getText().equals("")) {
                    servercall.getMessage("Hello, GWT!", new MyAsyncCallback(label));
                } else {
                    label.setText("");
                }
            }
        });



        buttonPanel.add(firstButton);
        buttonPanel.add(secondButton);
        buttonPanel.add(thirdButton);

        final VerticalPanel verticalPanel = new VerticalPanel();
        verticalPanel.add(deckPanel);
        verticalPanel.add(buttonPanel);
        verticalPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);

        RootPanel.get("gwtContainer").add(verticalPanel);
        RootPanel.get("slot1").add(button);
        RootPanel.get("slot2").add(label);
    }

    private static class MyAsyncCallback implements AsyncCallback<String> {
        private Label label;

        public MyAsyncCallback(Label label) {
            this.label = label;
        }

        public void onSuccess(String result) {
            label.getElement().setInnerHTML(result);
        }

        public void onFailure(Throwable throwable) {
            label.setText("Failed to receive answer from server!");
            //label.setText(throwable.toString());

        }
    }
}
