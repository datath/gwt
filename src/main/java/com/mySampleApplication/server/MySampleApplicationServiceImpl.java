package com.mySampleApplication.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.mySampleApplication.client.MySampleApplicationService;

public class MySampleApplicationServiceImpl extends RemoteServiceServlet implements MySampleApplicationService {

    public String getMessage(String msg) {
        String result;
        if (msg.equals("1")){
            result="This is the first page";
        } else if (msg.equals("2")) {
            result = "This is the second page";
        } else if (msg.equals("3")) {
            result = "This is the third page";
        } else  {
            result = "Client said: \"" + msg + "\"<br>Server answered: \"Hi!\"";
        }

        return result;
    }
}